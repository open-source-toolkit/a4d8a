# Unity PDF Renderer v5.15 资源文件介绍

## 概述

Unity PDF Renderer v5.15 是一个专为 Unity 引擎设计的插件，旨在帮助开发者轻松地在 Unity 项目中读取和渲染 PDF 文件。该插件提供了简单易用的接口，使得在游戏或应用程序中嵌入 PDF 文档变得非常方便。

## 功能特点

- **PDF 文件读取**：支持从本地或远程路径读取 PDF 文件。
- **PDF 渲染**：能够在 Unity 的 UI 系统中渲染 PDF 页面，支持缩放、平移等操作。
- **多平台支持**：兼容 Windows、macOS、Android 和 iOS 等多个平台。
- **性能优化**：经过优化，确保在不同设备上都能流畅运行。
- **易于集成**：提供详细的文档和示例代码，方便开发者快速上手。

## 使用方法

1. **导入插件**：将 Unity PDF Renderer v5.15 插件导入到你的 Unity 项目中。
2. **配置插件**：根据项目需求，配置插件的设置，如 PDF 文件路径、渲染参数等。
3. **调用接口**：在脚本中调用插件提供的接口，实现 PDF 文件的读取和渲染。
4. **运行项目**：在 Unity 编辑器或目标平台上运行项目，查看 PDF 文件的渲染效果。

## 示例代码

以下是一个简单的示例代码，展示如何在 Unity 中使用 Unity PDF Renderer v5.15 插件读取并渲染 PDF 文件：

```csharp
using UnityEngine;
using UnityPDFRenderer;

public class PDFViewer : MonoBehaviour
{
    public string pdfFilePath = "Assets/Documents/sample.pdf";

    void Start()
    {
        // 初始化 PDF 渲染器
        PDFRenderer renderer = new PDFRenderer();

        // 加载 PDF 文件
        renderer.LoadPDF(pdfFilePath);

        // 渲染第一页
        Texture2D pageTexture = renderer.RenderPage(0);

        // 将渲染的页面显示在 UI 上
        GetComponent<RawImage>().texture = pageTexture;
    }
}
```

## 注意事项

- 确保 PDF 文件路径正确，并且文件存在。
- 在移动平台上使用时，注意文件的读取权限。
- 如果遇到性能问题，可以尝试优化 PDF 文件的大小或调整渲染参数。

## 支持与反馈

如果你在使用过程中遇到任何问题或有任何建议，欢迎在 GitHub 仓库中提交 Issue 或 Pull Request。我们非常乐意为你提供帮助。

## 许可证

本项目采用 MIT 许可证，详情请参阅 [LICENSE](LICENSE) 文件。

---

希望 Unity PDF Renderer v5.15 能够帮助你在 Unity 项目中轻松实现 PDF 文件的读取和渲染！